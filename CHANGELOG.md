# Change Log

## [Unreleased]
- Initial release


# Changelog
All notable changes to this project will be documented in this file. Or not.

## [Unreleased]

## [0.0.1] - 2018-08-13
### Added
- Grammar injection for html templates in single quoted strings in JS files.
- Grammar injection for html templates in backtick quoted strings in JS files.
- Grammar injection for html templates in backtick quoted strings in PHP files.
- Grammar injection for LESS language in style tags with `type` attribute set to `text/less`. Ex: `<style type="text/less"></style>`
- Grammar injection for LESS language in style tags with `lang` attribute set to `less`. Ex: `<style lang="less"></style>`

## [0.0.2] - 2018-08-14
### Changed
- Grammar injections for html templates disabled in single quoted strings.

## [0.0.3] - 2018-08-14
### Added
- Icon

### Changed
- Changed display name to "Grammar injection. Html templates in backtick strings in JS and PHP files. Less Language in style tab with type attribute set to text/less and lang to less".

## [0.0.4] - 2018-08-14

### Changed
- Minor package extension fixes.

## [0.0.5] - 2020-02-11

### Added
- Added .vue files to list of file types to inject.

## [0.0.6] - 2020-02-11

### Changed
- Changed the regexp to include template starting with a word instead of a `<` like `Lorem ipsum <b>dolor</b>`
```
    "begin": "(?<=`)(?=<)",
	"end": "(?=`)",
```

## [0.1.0] - 2020-02-11

### Changed
- Changed the regexp to include template starting with a word instead of a `<` like `Lorem ipsum <b>dolor</b>`
```
    "begin": "(?<=`)(\\w| )*(?=<)",
	"end": "(?=`)",
```

## [0.1.1] - 2020-02-11

### Changed
- Changed the regexp to include template starting with a word instead of a `<` like `Lorem ipsum <b>dolor</b>`

```
    "begin": "(?<=`)[^`\n]*(?=<)",
	"end": "(?=`)",
```

## [0.1.2] - 2020-02-11

### Removed
- removed vsix file from repository