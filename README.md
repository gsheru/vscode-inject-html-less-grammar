# HTML & LESS grammar injections

This Visual Studio Code extension enables syntax hightlighting for HTML in template strings (backticks `"``"`) in `php`, `html`, `js` and `vue` files.

Also for LESS in template blocks in HTML and PHP files.

![php-example](https://gitlab.com/gsheru/vscode-inject-html-less-grammar/-/raw/master/img/php-example.png "PHP Example")

- Grammar injection for html templates in backtick quoted strings in JS files.
- Grammar injection for html templates in backtick quoted strings in PHP files.
- Grammar injection for LESS language in style tags with `type` attribute set to `text/less`. Ex: `<style type="text/less"></style>`
- Grammar injection for LESS language in style tags with `lang` attribute set to `less`. Ex: `<style lang="less"></style>`